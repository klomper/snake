package launch;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import snake.Game;

/**
 * Display.java - main klass, mille eesm�rk on m�ng k�ima t�mmata.
 * Kasutab AppGameContainer klassi et luua Graphical User Interface.
 * AppGameContainer v�tab sisse �he parameetri milleks on BasicGame klass.
 * 
 *
 * @author kerstin
 *
 */

public class Display {
	
	public static final String TITLE = "Snake";
	public static final int WIDTH = 640;
	public static final int HEIGHT = 480;
	
	public static void main(String[] args) {
		
		try {
			AppGameContainer appgc;
			appgc = new AppGameContainer(new Game(Display.TITLE));
			appgc.setDisplayMode(Display.WIDTH, Display.HEIGHT, false);
			appgc.setTargetFrameRate(60);
			appgc.setShowFPS(false);
			appgc.start();
		} catch (SlickException ex) {
			Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}
}

package snake.entety;

import org.newdawn.slick.Graphics;

/**
 * Ekraanil oleva objekti interface
 * @author kerstin
 *
 */
public interface Entety {
	public void render(Graphics g);
}

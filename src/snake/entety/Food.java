package snake.entety;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

import snake.Game;

/**
 * Food.java - klass, mis hoiab infot toidu kohta
 * @author kerstin
 *
 */
public class Food implements Entety {
	
	/**
	 * Toiduruuduke, mis hoiab infot
	 */
	private Rectangle bounds;
	private Color color;
	/**
	 * Klassi konstruktor
	 * @param x Esimene positsioon
	 * @param y Esimene positsioon
	 */
	public Food (float x, float y) {
		this.bounds = new Rectangle(x, y, Game.GRID, Game.GRID);
		this.color = new Color(173,255,47);
	}
	
	@Override
	/**
	 * Render meetod, joonistab toidu ekraanile
	 */
	public void render(Graphics g) {
		g.setColor(this.color);
		g.fill(this.bounds);
	}
	/**
	 * Annab toidule uued koordinaadid
	 * @param x annab uue x-koordinaadi
	 * @param y annab uue y-koordinaadi
	 */
	public void jump(float x, float y) {
		this.bounds.setLocation(x, y);
	}
	/**
	 * Tagastab toiduruudukese
	 * @return
	 */
	public Rectangle getRectangle() {
		return this.bounds;
	}

}

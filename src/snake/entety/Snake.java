package snake.entety;

import java.util.LinkedList;

import launch.Display;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

import snake.Game;

/**
 * Snake.java - klass, mis hoiab infot mao kohta
 * @author kerstin
 *
 */
public class Snake implements Entety {
	
	/**
	 * Mao liikumissuundade indeksid
	 */
	public static final int UP = 0;
	public static final int DOWN = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;
	
	/**
	 * Liikumissuunad
	 */
	private static final int[][] DIR = {{0, -1}, {0, 1}, {-1, 0}, {1, 0}};
	
	/**
	 * Hetkeline suund
	 */
	private int currentDir;
	/**
	 * J�rgnev suund
	 */
	private int nextDir;
	
	/**
	 * Mao pea
	 */
	private Rectangle head;
	private Color color;
	/** 
	 * Mao kehaosad
	 */
	private LinkedList<Rectangle> pieces;
	
	/**
	 * Mao kasvamine
	 */
	private boolean grow;
	/**
	 * Klassi konstruktor
	 * @param x Pea algne koordinaat
	 * @param y Pea algne koordinaat
	 */
	public Snake (float x, float y) {
		this.currentDir = Snake.UP;
		this.nextDir = -1; //-1 puudub
		this.head = new Rectangle(x, y, Game.GRID, Game.GRID);
		this.pieces = new LinkedList<Rectangle>();
		this.color = new Color(255,20,147);
		
		
		//Algne mao pikkus
		for (int i = 0; i < 4; i++) {
			this.pieces.add(new Rectangle(x, y, Game.GRID, Game.GRID));
		}
		
		this.grow = false;
	}
	
	@Override
	/**
	 * Render meetod, joonistab mao ekraanile
	 */
	public void render(Graphics g) {
		g.setColor(this.color);
		g.fill(this.head);
		for (Rectangle r:this.pieces) {
			g.fill(r);
		}
	}
	/**
	 * Mao liikumise loogika
	 */
	public void update() {
		
		if (this.nextDir != -1) {
			this.currentDir = this.nextDir;
			this.nextDir = -1;
		}
		
		this.pieces.add(this.head);
		
		if (!this.grow) {
			this.pieces.pop();
		} else {
			this.grow = false;
		}
		
		float x = this.calcX();
		float y = this.calcY();
		this.head = new Rectangle(x, y, Game.GRID, Game.GRID);
		
	}
	/**
	 * Kontrollib, kas madu s�i toidu �ra v�i ei
	 * @param r Toidukastike
	 * @return tagastab, kas s�i v�i ei s��nud
	 */
	public boolean eats(Rectangle r) {
		if (this.head.getX() == r.getX() && this.head.getY() == r.getY()) {
			return true;
		}
		return false;
	}
	/**
	 * Kontrollib, kas madu s�idab endale otsa
	 * @return tagastab, kas s�itis v�i ei
	 */
	public boolean bite() {
		for (Rectangle r:this.pieces) {
			if (this.head.getX() == r.getX() && this.head.getY() == r.getY()) {
				return true;
			}
		}
		return false;
	}
	/**
	 * Kasvatab mao �he ruudukese v�rra sellel m�nguts�klil, tickil
	 */
	public void grow() {
		this.grow = true;
	}
	/**
	 * Kui ei ole vastassuund, siis vahetab suunda
	 * @param direction suund
	 */
	public void changeDirection(int direction) {
		if (this.isOpposite(direction)) {
			return;
		}
		this.nextDir = direction;
	}
	/**
	 * Kontrollib, kas on vastassuund
	 * @param direction suund
	 * @return tagastab, kas oli vastassuund
	 */
	private boolean isOpposite(int direction) {
		return (Snake.DIR[this.currentDir][0] + Snake.DIR[direction][0]) == 0 && (Snake.DIR[this.currentDir][1] + Snake.DIR[direction][1]) == 0;
	}
	/**
	 * Arvutab uued mao pea koordinaadid
	 * @return tagastab uue X
	 */
	private float calcX() {
		float x = this.head.getX() + Snake.DIR[this.currentDir][0] * Game.GRID;
		if (x >= Display.WIDTH) {
			x = 0;
		} else if (x < 0) {
			x = Display.WIDTH - Game.GRID;
		}
		return x;
	}
	/**
	 * Arvutab uued mao pea koordinaadid
	 * @return tagastab uue Y
	 */
	private float calcY() {
		float y = this.head.getY() + Snake.DIR[this.currentDir][1] * Game.GRID;
		if (y >= Display.HEIGHT) {
			y = 0;
		} else if (y < 0) {
			y = Display.HEIGHT - Game.GRID;
		}
		return y;
	}
}

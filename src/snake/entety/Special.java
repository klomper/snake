package snake.entety;

import launch.Display;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

import snake.Game;

/**
 * Special.java - klass, mis hoiab infot boonuse kohta
 * Laiendab food klassi
 * @author kerstin
 *
 */
public class Special extends Food {
	
	private Color color;
	/**
	 * Kui kaua boonus n�htaval on
	 */
	private int maxTime;
	/**
	 * Jooksev aeg
	 */
	private int timer;
	/**
	 * Boonuse aeg l�bi
	 */
	private boolean timeout;
	/**
	 * Boonus on n�htaval
	 */
	private boolean visible;
	/**
	 * Ekraani keskpunkt, et selle j�rgi v�lja arvutada, kuhu poole tuleb timer
	 */
	private float centerX;
	private float centerY;
	
	/**
	 * Arvutab v�lja timeri maksimum aja game ticki, ekraani suuruse ja game gridi suhtes
	 * Initsialiseerib muutujad
	 * @param x boonuse algkoordinaat
	 * @param y boonuse algkoordinaat 
	 */
	public Special(float x, float y) {
		super(x, y);
		this.maxTime = ((Display.WIDTH / Game.GRID + Display.HEIGHT / Game.GRID)) * Game.TICK;
		this.timeout = false;
		this.visible = false;
		this.centerX = Display.WIDTH / 2;
		this.centerY = Display.HEIGHT / 2;
		this.color = new Color(250,128,114);
	}
	/**
	 * N�itab boonust, seab timeri maksimum ajale
	 */
	public void show() {
		this.visible = true;
		this.timer = this.maxTime;
		this.timeout = false;
	}
	/**
	 * Peidab boonuse
	 */
	public void hide() {
		this.visible = false;
	}
	/**
	 * Kas on n�htav 
	 * @return tagastab kas on n�htav v�i ei ole 
	 */
	public boolean isVisible() {
		return this.visible;
	}
	/**
	 * Kas on aeg l�bi v�i ei
	 * @return tagastab kas on aeg l�bi v�i ei
	 */
	public boolean isTimeout() {
		return this.timeout;
	}
	
	@Override
	/**
	 * Render meetod, joonistab boonuse ekraanile
	 * Kirjutab timeri ekraanile
	 */
	public void render(Graphics g) {
		g.setColor(this.color);
		if (this.visible) {
			Rectangle r = this.getRectangle();
			String time = String.format("%05.2f", ((float)this.timer)/1000); //% t�hendas muutujat, 05 t�hendab 5 nr kokku seal komaga koos arvutatud, kui panna 06 siis on �ks null juures alguses ja .2 n�itab mitu kohta p�rast koma on n�htaval ja f on float
			float x = this.getStringX(r, g.getFont().getWidth(time));
			float y = this.getStringY(r, g.getFont().getHeight(time));
			g.fill(r);
			g.drawString(time, x, y);
		}
	}
	/**
	 * Uuendab timerit, kui on n�htav
	 * @param delta m��dunud aeg eelmisest framest
	 */
	public void update(int delta) {
		if (this.visible) {
			this.timer -= delta;
			if (this.timer <= 0) {
				this.timeout = true;
			}
		}
	}
	/**
	 * Arvutab timeri X positsiooni
	 * @param r boonuse kastike
	 * @param lenght timeri teksti pikkus
	 * @return tagastab timeri x positsiooni
	 */
	private float getStringX(Rectangle r, int lenght) {
		
		float x = r.getX();
		
		if (x < this.centerX) {
			x += Game.GRID;
		} else {
			x -= lenght;
		}
		
		return x;
	}
	/**
	 * Arvutab timeri Y positsiooni
	 * @param r boonuse kastike
	 * @param height timeri teksti k�rgus
	 * @return tagastab timeri y positsiooni
	 */
	private float getStringY(Rectangle r, int height) {
		
		float y = r.getY();
		
		if (y < this.centerY) {
			y += Game.GRID;
		} else {
			y -= height;
		}
		
		return y;
	}
}

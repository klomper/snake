package snake;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Random;

import launch.Display;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

import snake.entety.Entety;
import snake.entety.Food;
import snake.entety.Snake;
import snake.entety.Special;

/**
 * Game.java - klass, kus on kogu m�nguloogika. 
 * @author kerstin
 *
 */
public class Game extends BasicGame {
	
	/**
	 * Mitu pikslit on �hes m�nguruudus
	 */
	public static final int GRID = 20;  
	/**
	 * Kui tihti m�nguloogika jookseb/kui kiiresti liigub
	 */
	public static final int TICK = 100;
	
	public static final String GO = "Game Over";
	
	private Random random;
	private int gridX;
	private int gridY;
	
	/**
	 * Ticki timer
	 */
	private int timer; 
	/**
	 * Jooksev skoor
	 */
	private int score;
	/**
	 * Kui tihti tuleb boonus (countdown kui palju valgeid on j��nud boonuseni, iga kord saab eri v��rtuse)
	 */
	private int specialCountdown;
	/**
	 * M�ng l�bi
	 */
	private boolean gameover;
	/**
	 * Madu
	 */
	private Snake snake;
	/**
	 * Toit
	 */
	private Food food;
	/**
	 * Boonus/special, mis iga m�ne aja tagant ilmub
	 */
	private Special special;
	/**
	 * Entety list
	 */
	private ArrayList<Entety> enteties;
	private Font font;
	private TrueTypeFont ttf;
	private Image background;
	/**
	 * Game overi tekstikoordinaadid
	 */
	private float goCenterX;
	private float goCenterY;
	
	/**
	 * Game overi tekstikoordinaadid
	 * @param title
	 */
	public Game(String title) {
		super(title);
		this.random = new Random(System.currentTimeMillis());
		this.gridX = Display.WIDTH / Game.GRID;
		this.gridY = Display.HEIGHT / Game.GRID;
		this.reset();
	}

	@Override
	/**
	 * Render joonistab graafika ekraanile
	 */
	public void render(GameContainer gc, Graphics g) throws SlickException {
		
		this.background.draw();
		if (this.gameover) {
			g.setColor(Color.red);
			this.ttf.drawString(this.goCenterX, this.goCenterY, Game.GO);
			return;
		}
		
		for (Entety e:this.enteties) {
			e.render(g);
		}
		g.setColor(Color.white);
		g.drawString(String.format("Score: %05d", this.score), 10, 10);
	}

	@Override
	/**
	 * Initsialiseerib muutujad
	 */
	public void init(GameContainer gc) throws SlickException {
		this.font = new Font("Verdana", Font.BOLD, 32);
		this.ttf = new TrueTypeFont(font, true);
		this.goCenterX = (Display.WIDTH - ttf.getWidth(Game.GO)) / 2;
		this.goCenterY = (Display.HEIGHT - ttf.getHeight(Game.GO)) / 2;
		this.background = new Image("res/background.png");
	}

	@Override
	/**
	 * M�nguloogika
	 */
	public void update(GameContainer gc, int delta) throws SlickException {
		
		
		//Loeb kasutaja klahvisisestused
		this.readInput(gc.getInput());
		
		
		//Kontrollib kas m�ng on l�bi
		if (this.gameover) {
			return;
		}
		
		
		//Uuendab iga frame specialit
		 this.special.update(delta);
		
		//Uuendab m�nguloogikat iga intervalli tagant (Tick)
		 if (this.tick(delta)) {
			
			//Kontrollib, kas madu s�i toidu �ra, uus asukoht toidule, madu pikeneb ja skoor juurde, countdown specialtoidule v�heneb
			if (this.snake.eats(this.food.getRectangle())) {
				this.food.jump(this.rX(), this.rY());
				this.snake.grow();
				this.score += 10;
				this.specialCountdown--;
			}
			
			//Kui countdown j�uab nulli, ilmub special ekraanile ja see saab uue v��rtuse
			if (this.specialCountdown == 0) {
				this.specialCountdown = this.sC();
				this.special.show();
			}
			/*
			 * Kontrollib, kas kui special on n�htaval, madu s�i selle �ra, special kaob �ra ja hiljem saab uue asukoha, skoor juurde
			 * Kui madu ei s��nud �ra specialit ja aeg saab otsa, special kaob �ra, saab hiljem uue asukoha
			 */
			if (this.special.isVisible() && this.snake.eats(this.special.getRectangle())) {
				this.special.hide();
				this.special.jump(this.rX(), this.rY());
				this.score += 50;
			} else if (this.special.isVisible() && this.special.isTimeout()) {
				this.special.hide();
				this.special.jump(this.rX(), this.rY());
			}
			this.snake.update();
			
			//Kui madu s�idab endale otsa, game over
			if (this.snake.bite()) {
				this.gameover = true;
			}
		}
	}
	/**
	 * Loeb kasutaja klahvisisestused
	 * @param input klass, mis loeb user inputi
	 */
	private void readInput(Input input) {
		
		
		//Space vajutades algab m�ng otsast peale 
		if (input.isKeyPressed(Input.KEY_SPACE)) {
			this.reset();
		}
		
		//Exitit vajutades v�ljub m�ngust
		if (input.isKeyPressed(Input.KEY_ESCAPE)) {
			System.exit(0);
		}
		
		//Kustutab m�lu, nullib k�ik muud klahvivajutused
		if (this.gameover) {
			input.clearKeyPressedRecord();
			return;
		}
		
		//Nool �les muudab suuna �les
		if (input.isKeyPressed(Input.KEY_UP)) {
			this.snake.changeDirection(Snake.UP);
		}
		
		//Nool alla muudab suuna alla
		if (input.isKeyPressed(Input.KEY_DOWN)) {
			this.snake.changeDirection(Snake.DOWN);
		}
		
		//Nool vasakule muudab suuna vasakule
		if (input.isKeyPressed(Input.KEY_LEFT)) {
			this.snake.changeDirection(Snake.LEFT);
		}
		
		//Nool paremale muudab suuna paremale
		if (input.isKeyPressed(Input.KEY_RIGHT)) {
			this.snake.changeDirection(Snake.RIGHT);
		}
		input.clearKeyPressedRecord();
	}
	/**
	 * Kontrollib, kas �he ticki intervall on t�is, kui on siis returnib true ja kui ei siis returnib false
	 * @param delta m��dunud aeg eelmisest framest
	 * @return 
	 * 		true kui game tick t�is saab
	 * 		false kui ei ole t�is saanud
	 */ 
	private boolean tick(int delta) {
		this.timer += delta;
		if (this.timer >= Game.TICK) {
			this.timer -= Game.TICK;
			return true;
		}
		return false;
	}
	/**
	 * Nullib k�ik muutujad(m�ngu k�ima pannes ja uut m�ngu alustades)
	 */
	public void reset() {
		this.timer = 0;
		this.score = 0;
		this.specialCountdown = this.sC();
		this.gameover = false;
		
		this.snake = new Snake(this.rX(), this.rY());
		this.food = new Food(this.rX(), this.rY());
		this.special = new Special(this.rX(), this.rY());
		
		this.enteties = new ArrayList<Entety>();
		this.enteties.add(this.food);
		this.enteties.add(this.special);
		this.enteties.add(this.snake);
	}
	/**
	 * Arvutab gridis suvalise x positsiooni
	 * @return (0-ruutude arv)*pikslid ruudus
	 */
	private float rX() {
		return this.random.nextInt(this.gridX) * Game.GRID;
	}
	/**
	 * Arvutab gridis suvalise y positsiooni
	 * @return (0-ruutude arv)*pikslid ruudus
	 */
	private float rY() {
		return this.random.nextInt(this.gridY) * Game.GRID;
	}
	/**
	 * Speciali countdown
	 * @return suvalise arvu 3 ja 8 vahel 
	 *
	 */
	private int sC() {
		return this.random.nextInt(5) + 3;
	}
}
